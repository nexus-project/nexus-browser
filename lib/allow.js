const readFileSync = require('fs').readFileSync;
const ipRangeCheck = require('ip-range-check');

function load(filename) {
  let file = null;
  try {
    file = readFileSync(__dirname + '/../' + filename, 'utf8');
  } catch (e) {
    return [];
  }
  return file
    .split('\n')
    .map(_ => _.trim())
    .filter(_ => !_.startsWith('#'))
    .filter(Boolean);
}

const allow = load('allow');
const block = load('block');

const ipRE = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;

module.exports = (hostname = '') => {
  hostname = hostname.toLowerCase().trim();

  // shouldn't happen, but…
  if (!hostname) {
    return false;
  }

  // don't allow IPv6 addresses
  if (hostname.includes(':')) {
    return false;
  }

  // check the hostname against the blacklist
  if (block.includes(hostname)) {
    return false;
  }

  // don't allow hostnames without a domain name
  if (!hostname.includes('.')) {
    return false;
  }

  // don't allow "cern.ch" or ".cern" domains, unless explicitly whitelisted
  if (hostname.endsWith('cern.ch') || hostname.endsWith('cern.ch.') || hostname.endsWith('.cern')) {
    return allow.includes(hostname);
  }

  //Default value, rejected!
  return false;
};
