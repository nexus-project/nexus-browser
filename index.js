require('@remy/envy');
const express = require('express');
const path = require('path');
const app = express();

app.disable('x-powered-by');
app.use((req, res, next) => {
  res.setHeader('X-Powered-By', '1990s technology, and unicorns, obviously');
  next();
});

// Serve static files from build/browser directory
app.use('/browser', express.static(path.join(__dirname, 'build', 'browser')));
app.use('/api', require('./lib/api'));
app.use('/', express.static('./build/www'));

// Handle direct requests to /browser
app.get('/browser', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'browser', 'index.html'));
});

if (process.env.NODE_ENV === 'development') {
  app.use('/www/', express.static('./src/assets/www'));
}

const server = app.listen(process.env.PORT || 3000, () => {
  console.log(`listening http://localhost:${server.address().port}`);
});